<?php

namespace App\Http\Controllers\Employees;

use App\Http\Controllers\Controller;
use App\Imports\EmployeesImport;
use App\Models\Employe\Employe;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class EmployeesController extends Controller
{
    public function index(){
        $employees = Employe::latest()->get();

        foreach ($employees as $key => $item) {
            QrCode::size(500)
            ->format('png')
            ->color(224, 11, 14)
            //->backgroundColor(226, 111, 12)
            ->generate($item["cedula"], storage_path("app/public/img/qrcode/".$item['cedula'].".png"));
        }
        
        return Inertia("qrgenerator/index", [
            "employees" => $employees
        ]);
    }

    public function upload(Request $request){
        
        if($request->file){
           try {
                Excel::import(new EmployeesImport, request()->file("file"));
           } catch (\Throwable $th) {
               throw $th;
           }
        }
        
    }
}
