<?php

use App\Http\Controllers\Employees\Employees;
use App\Http\Controllers\Employees\EmployeesController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Dashboard', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
})->middleware(['auth:sanctum', 'verified']);

Route::middleware(['auth:sanctum', 'verified'])->get('/qr-generator', function () {
    return Inertia::render('Dashboard');
})->name('qrgenerator');


Route::middleware(['auth:sanctum', 'verified'])->post('qr-generator/upload', [EmployeesController::class, "upload"])
->name("qrgenerator.upload");

Route::middleware(['auth:sanctum', 'verified'])->get('qr-generator/listado', 
[EmployeesController::class, "index"])
->name("qrgenerator.index");
