<?php
namespace App\Imports;

use App\Models\Employe\Employe;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class EmployeesImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        //dd($row);
        $Employe = Employe::create([
            'name' => $row[0],
            'cedula' => $row[1]
        ]);
        return $Employe;
       
    }
}
